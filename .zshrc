# Path to your oh-my-zsh installation.
export ZSH=/Users/username/.oh-my-zsh

function export-nvm {
  export NVM_DIR="$HOME/.nvm"
  . "/usr/local/opt/nvm/nvm.sh"
}

function eval-docker {
  eval $(docker-machine env $1)
}

function docker-rm-container {
  docker rm $(docker ps -a -q)
}

function docker-rm-images {
    docker rmi -f $(docker images -q)
}

function docker-kill-all {
  docker kill $(docker ps -q)
}

MAILCHECK=0

# Brew update
alias brewup='brew update; brew upgrade; brew prune; brew cleanup; brew doctor'

# TheFuck
eval $(thefuck --alias)

# Aliases
alias cl=clear
alias dc=docker-compose
alias dm=docker-machine
alias dka=docker-kill-all
alias fu='fuck -y'

test -e "${HOME}/.iterm2_shell_integration.zsh" && source "${HOME}/.iterm2_shell_integration.zsh"

source $ZSH/oh-my-zsh.sh
source /usr/local/share/zsh-autosuggestions/zsh-autosuggestions.zsh

[ -f ~/.fzf.zsh ] && source ~/.fzf.zsh
