# Docker environement setup

## Virtualbox installation
```bash
$ brew cask install virtualbox
```

## Docker, Docker Machine, Docker Compose installation
```bash
$ brew install docker && brew install docker-machine && brew install docker-compose
```

## Default docker-machine setup
```bash
$ docker-machine create -d virtualbox --virtualbox-cpu-count "2" --virtualbox-disk-size "100000" --virtualbox-memory "4096" default
```

## NFS setup
Install `adlogix/docker-machine-nfs`:
```bash
$ brew install docker-machine-nfs
$ sudo nfsd checkexports && sudo nfsd restart
$ docker-machine-nfs default --shared-folder=/Users --nfs-config="-alldirs -maproot=0" --mount-opts="async,noatime,actimeo=1,nolock,vers=3,udp" -f
```
