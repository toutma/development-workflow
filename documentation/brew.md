# Brew
##  List of all your installed Homebrew packages
```bash
$ brew list
ack				git				libuv				php				selenium-server-standalone
apr				git-standup			libvterm			php56				shellcheck
apr-util			glib				libxml2				php70				sqlite
argon2				gmp				libyaml				php71				the_silver_searcher
aspell				go				libzip				php72				thefuck
autoconf			highlight			lua				php@5.6				trash
cscope				hub				luajit				php@7.0				tree
ctags				icu4c				markdown			php@7.1				unibilium
diff-so-fancy			imap-uw				mas				pkg-config			unixodbc
direnv				jemalloc			mcrypt				python				vim
dnsmasq				jpeg				mhash				python3				webp
docker-machine			libevent			mkcert				python@2			wget
docker-machine-nfs		libffi				msgpack				rbenv				xz
docker-machine-parallels	libidn2				neovim				rbenv-default-gems		z
entr				libpng				nginx				rbenv-gemset			zplug
freetds				libpq				node				readline			zsh
freetype			libsodium			nvm				reattach-to-user-namespace	zsh-autosuggestions
fzf				libtermkey			openssl				ripgrep
gdbm				libtool				pcre				ruby
gettext				libunistring			perl				ruby-build
```

## Items installed using the Caskroom
```bash
$ brew cask list
betterzipql            gitscout               ngrok                  qlprettypatch          quicklook-json         suspicious-package     
qlcolorcode            qlstephen              macvim                 qlmarkdown             quickloo               webpquicklook
```
