# MAC OS SETUP

## Prerequites
* Brew
* VirtualBox
* Docker
* iTerm2

## iTerm2


## Oh-my-zsh
[zshrc config](/documentation/zshrc.md)


## Brew
[Brew setup](/documentation/brew.md)


## Docker
[Docker setup](/documentation/docker.md)


## Git
[Git config](/documentation/gitconfig.md)
[Git global config](/documentation/gitconfig_global.md)

